import logging.config

from flask import Flask, jsonify, request, render_template

from constant import FILE_PATH
from repository import ElasticSearchRepository

app = Flask(__name__)
logging.config.fileConfig(fname='utils/logging_to_file.conf')
logger = logging.getLogger(__name__)


@app.route('/', methods=['GET'])
def index():
    repo = ElasticSearchRepository()
    if not repo.check_index_exists():
        repo.load_data(FILE_PATH)
    return render_template('index.html')


@app.route('/insert_data', methods=['POST'])
def insert_data():
    repo = ElasticSearchRepository()
    result = repo.create(request.form['word'])

    return render_template('result.html', create_result=result['result'], word=request.form['word'])


@app.route('/search', methods=['POST'])
def search():
    repo = ElasticSearchRepository()
    result = repo.search(request.form['word'])
    return render_template('result.html', search_result=result, word=request.form['word'])


@app.route('/delete', methods=['POST'])
def delete():
    repo = ElasticSearchRepository()
    word, result = repo.delete(request.form['word'])
    if type(result) == 'string':
        return render_template('result.html', delete_result=result)
    else:
        return render_template('result.html', delete_result=result['deleted'], word=word)


