import abc
import logging.config

from elasticsearch import Elasticsearch, helpers
from constant import INDEX_CONSTANT, ELASTICSEARCH_URI

logging.config.fileConfig(fname='utils/logging_to_file.conf')
logger = logging.getLogger(__name__)


class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def search(self, word: str):
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, word: str):
        raise NotImplementedError

    @abc.abstractmethod
    def create(self, word: str):
        raise NotImplementedError


class ElasticSearchRepository(AbstractRepository):
    def __init__(self):
        self.es = Elasticsearch(ELASTICSEARCH_URI)
        try:
            self.es.info()
        except Exception as exp:
            logger.error("Error connecting Elastic Search. Stack trace "+ str(exp), exc_info=True)
        else:
            logger.info("Elastic Search Connected")

    def load_data(self, file_path):
        bulk_data = []
        try:
            with open(file_path, 'r') as f:
                lines = set(f.readlines())
                for line in lines:
                    for word in line.split():
                        bulk_data.append({'word': word})
                        if len(bulk_data) == 1000:
                            helpers.bulk(self.es, bulk_data, index=INDEX_CONSTANT)
                            bulk_data = []
        except Exception as exp:
            logger.error("Error connecting Elastic Search. Stack trace "+ str(exp), exc_info=True)
        else:
            logger.info("Load data from {} succeeded".format(file_path))

    def create(self, word: str):
        res = self.es.index(
            index=INDEX_CONSTANT,
            document={
                'word': word
            }
        )
        return res.body

    def search(self, word: str):
        res = self.es.search(
            index=INDEX_CONSTANT,
            body={
                "query": {
                    "match": {
                        "word": {
                            "query": word,
                            "fuzziness": 'AUTO',
                        }
                    }
                }
            },
            ignore=[400, 404]
        )

        return res.body

    def delete(self, word: str):
        result = self.search(word)
        hits = result['hits']
        if hits['total']['value'] > 0:
            for hit in hits['hits']:
                if hit['_score'] == hits['max_score']:
                    query = {"query": {"match": {"word": hit['_source']['word']}}}
                    res = self.es.delete_by_query(index=INDEX_CONSTANT, body=query)
                    return hit['_source']['word'], res.body
        else:
            return word, 'Word not found'

    def delete_index(self):
        result = self.es.indices.delete(index=INDEX_CONSTANT)
        return result.body

    def check_index_exists(self):
        if self.es.indices.exists(index=INDEX_CONSTANT):
            return True
        return False

